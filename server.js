/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes');

var mysql = require('mysql');
var md5 = require('MD5');
var crypto = require('crypto');
 var sanitizer = require('sanitizer');

var app = express();

var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;
  
  
var sqlInfo = {
    host: 'localhost', 
    user: 'root',
    password: '3jal1ma6', 
    database: 'yuanshitou'
}

global.client = mysql.createConnection(sqlInfo);
 
client.connect();


// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.static(__dirname + '/public'));
  app.use(express.cookieParser("secret"));
    app.use(express.session({
        secret: 'keyboard cat'
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(app.router);
 
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});


passport.serializeUser(function(user, done) {
    console.log(user[0].idUser);
    done(null, user[0].idUser);
});

passport.deserializeUser(function(id, done) {

    client.query('SELECT * FROM users WHERE idUser = ?', [id], function(err, user)
    {
        done(err,user);
    });
});


passport.use(new LocalStrategy({
     usernameField: 'email',
     passwordField: 'password'
},
    function(username, password, done) {
        console.log('local strategy called');
        console.log(username);
        client.query('SELECT * FROM users WHERE email = ?', [username], function(err, results)
        {
            console.log(err);
            console.log(results);
            if (results.length == 0)
            {
                return done(null, false, false);
            }
            else
            {
                console.log("Password store in md5: "+results[0].password);
                var newPassword  = require("crypto").createHash("md5").update(sanitizer.escape(password)).digest("hex");
                console.log("Password created in md5: "+newPassword);
                if (results[0].password === newPassword){
                    console.log("OK");
                    return done(null, results);
                }
                else{
                    return done(null, false, false);
                }
            }
        });
    }
));


function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    res.redirect('/login')
}
// Routes

app.get('/', routes.index);
app.get('/register', routes.register);
app.get('/signin', routes.signin);
app.get('/dashboard', ensureAuthenticated, function(req, res) {
  res.render('dashboard/index', { title: '仪表盘' })
});
app.post('/register', routes.registerUser);
app.post('/signin', function(req, res, next) {
 
    passport.authenticate('local', { successRedirect: '/dashboard',
        failureRedirect: '/' })(req, res, next);

});

app.listen(8089, function(){
  //console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});

